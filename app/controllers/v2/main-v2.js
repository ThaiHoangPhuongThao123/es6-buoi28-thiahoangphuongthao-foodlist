import { Food } from "../../models/v2/model.js";
import {
  layTHongTinTuForm,
  renderFoodList,
  showThongTinLenForm,
} from "./controller-v2.js";

const BASE_URL = `https://643a58d090cd4ba563f774dc.mockapi.io/food`;
let idEL = null;

function fetchArr() {
  axios({
    url: BASE_URL,
    method: "GET",
  }).then((res) => {
    let newArrFood = res.data.map((item) => {
      let { id, name, type, discount, img, desc, price, status } = item;
      let food = new Food(id, name, type, price, discount, status, img, desc);
      return food;
    });
    renderFoodList(newArrFood);
  });
}
fetchArr();
window.deleteFood = (id) => {
  axios({
    url: BASE_URL + `/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log("res: ", res);
      fetchArr();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

window.editFood = (id) => {
  idEL = id;

  axios({
    url: BASE_URL + `/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      let { id, name, type, discount, img, desc, price, status } = res.data;
      let food = new Food(id, name, type, price, discount, status, img, desc);
      showThongTinLenForm(food);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

window.themMon = () => {
  let data = layTHongTinTuForm();
  let newFood = {
    id: data.foodID,
    name: data.tenMon,
    type: data.loai,
    price: data.giaMon,
    discount: data.khuyenMai,
    status: data.tinhTrang,
    img: data.hinhMon,
    desc: data.moTa,
  };
  axios({
    url: BASE_URL,
    method: "POST",
    data: newFood,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");

      console.log("res: ", res);
      fetchArr();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

window.capNhap = () => {
  let data = layTHongTinTuForm();
  let newFood = {
    id: data.foodID,
    name: data.tenMon,
    type: data.loai,
    price: data.giaMon,
    discount: data.khuyenMai,
    status: data.tinhTrang,
    img: data.hinhMon,
    desc: data.moTa,
  };
  axios({
    url: BASE_URL + `/${idEL}`,
    method: "PUT",
    data: newFood,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchArr();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

window.sapXep = () => {
  let loai = document.getElementById("selLoai").value;
  console.log('loai: ', loai);
  let newArrFood = [];
  axios({
    url: BASE_URL,
    method: "GET",
  }).then((res) => {
    if ((loai == "all")) {
      let newArrFood = res.data.map((item) => {
      let { id, name, type, discount, img, desc, price, status } = item;
      let food = new Food(id, name, type, price, discount, status, img, desc);
      return food;
    });
    renderFoodList(newArrFood);
    } else if ((loai == "loai1")) {
      res.data.forEach((item) => {
        let { id, name, type, discount, img, desc, price, status } = item;
        let food = new Food(id, name, type, price, discount, status, img, desc);
        if (type == "loai1" || type) {
          newArrFood.push(food);
        }
        renderFoodList(newArrFood);
      });
    } else if ((loai == "loai2")) {
      res.data.forEach((item) => {
        let { id, name, type, discount, img, desc, price, status } = item;
        let food = new Food(id, name, type, price, discount, status, img, desc);
        if (type == "loai2" || type == false) {
          newArrFood.push(food);
        }
        renderFoodList(newArrFood);
      });
    }
  });
};
