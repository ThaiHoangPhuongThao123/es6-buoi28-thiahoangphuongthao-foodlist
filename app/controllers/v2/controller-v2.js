export function renderFoodList(arrFood) {
  let contenHTML = ``;
  arrFood.forEach((item) => {
    let { foodID, tenMon, loai, giaMon, khuyenMai, tinhTrang } = item;
    if ((loai == true) || (loai == "loai1")) {
        loai = "Chay";
    } else {
        loai = "Mặn"
    }
    if ((tinhTrang == true) || (tinhTrang == 1)) {
        tinhTrang = "Còn";
    } else {
        tinhTrang = "Hết"
    }
    let contentRow = `
    <tr>
    <td>${foodID}</td>
    <td>${tenMon}</td>
    <td>${loai } </td>
    <td>${giaMon}</td>
    <td>${khuyenMai} </td>
    <td>${item.tinhGiaKM()}</td>
    <td>${tinhTrang}  </td>
    <td> <button class="btn btn-success" onclick ="editFood('${foodID}')">Edit</button>
    <button class="btn btn-danger" onclick ="deleteFood('${foodID}')">Delete</button></td>
    </tr>
    `;
    contenHTML += contentRow;
  });
  document.getElementById("tbodyFood").innerHTML = contenHTML;
}
export function showThongTinLenForm(item) {
  let { foodID, tenMon, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } =
    item;
  document.getElementById("foodID").value = foodID;
  document.getElementById("tenMon").value = tenMon;
  document.getElementById("loai").value = ((loai == true) || (loai == "loai1"))? "loai1" : "loai2";
  console.log('loai: ', loai);
  document.getElementById("giaMon").value = giaMon;
  document.getElementById("khuyenMai").value = khuyenMai;
  document.getElementById("tinhTrang").value = ((tinhTrang == true) || (tinhTrang == 1)) ? 1 : 0;
  document.getElementById("hinhMon").value = hinhMon;
  document.getElementById("moTa").value = moTa;
}

export function layTHongTinTuForm() {
  let foodID = document.getElementById("foodID").value;
  let tenMon = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value * 1;
  let khuyenMai = document.getElementById("khuyenMai").value * 1;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;
  return {
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
}
